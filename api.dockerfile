# docker build -f api.dockerfile .
FROM node:alpine

WORKDIR /app

COPY packages/api/package.json /app/package.json

RUN npm install

COPY packages/api .

ENV NODE_ENV production
ENV PORT 80

EXPOSE 80

CMD npm  start
