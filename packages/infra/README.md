# Infra

## Prerequisits

Identify your google project name

```sh
export PROJECT_ID=block8-cwaa
export TF_VAR_gitlab_token=yourtoken
```

- CLI tools
  - `gcloud`
  - `kubectl`
  - `helm`
- GCP Project with the ID set to `$PROJECT_ID`
- And a GCP bucket called `$PROJECT_ID-tf-state`

## 1. GCloud

### 1.1 Authenticate

```sh
# First time? You need to init
gcloud init

# Otherwise, just switch context
gcloud config configurations activate my_config

```

```sh
gcloud auth application-default login
```

_Note; see 1.2 and 2.3 for docker and helm auth respectively_

### 1.2 Configure `docker`

```sh
gcloud auth configure-docker --project $PROJECT_ID
```


### 2. Manually Update Values

- Update variables in `.gitlab-ci.yml`
- Update traefik hard coded wildcard domain `packages/infra/traefik-chart/templates/config.yaml`
- Update terraform state bucket
- Update App chart values `./packages/infra/app-chart/values.yaml`
- Update Skaffold `./skaffold.yaml`


## 3. Terraform

### 3.1 Setup

First update `./terraform.tfvars` with correct values.

```sh
terraform init ./packages/infra/terraform
```

### 3.2 Apply

This command may be used to setup first time, and to perform updates thereafter.

```sh
terraform apply ./packages/infra/terraform
```

### 3.3 Configure `kubectl`

```sh
gcloud beta container clusters get-credentials primary-app-cluster --region australia-southeast1 --project $PROJECT_ID
```

## 4. Release Application

```sh
export RELEASE_TAG=${"$(git branch | grep \* | cut -d ' ' -f2)"/\//-}
```

### 4.1 Build and push docker images

```sh
docker build -t gcr.io/$PROJECT_ID/web:$RELEASE_TAG -f web.dockerfile . &&\
docker push gcr.io/$PROJECT_ID/web:$RELEASE_TAG

docker build -t gcr.io/$PROJECT_ID/api:$RELEASE_TAG -f api.dockerfile . &&\
docker push gcr.io/$PROJECT_ID/api:$RELEASE_TAG

docker build -t gcr.io/$PROJECT_ID/storybook:$RELEASE_TAG -f storybook.dockerfile . &&\
docker push gcr.io/$PROJECT_ID/storybook:$RELEASE_TAG
```

### 4.2 Release application via helm

```sh
helm upgrade --install $RELEASE_TAG ./packages/infra/app-chart \
--set apiImage=gcr.io/$PROJECT_ID/api:$RELEASE_TAG \
--set webImage=gcr.io/$PROJECT_ID/web:$RELEASE_TAG \
--set storybookImage=gcr.io/$PROJECT_ID/storybook:$RELEASE_TAG
```

### 4.3 Observe your work

```sh
open https://$RELEASE_TAG.basedomain.com
```
