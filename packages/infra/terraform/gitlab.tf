resource "google_service_account" "gitlab-ci" {
  account_id   = "gitlab-ci-gcr-push"
  display_name = "GitLab CI - GCR Push"
}

resource "google_project_iam_custom_role" "ci" {
  role_id     = "gitlabci"
  title       = "CI / CD"
  description = "Access for Continuous Integration and Deployment"
  permissions = ["container.clusters.get"]
}

resource "google_project_iam_member" "ci-storage" {
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.gitlab-ci.email}"
}

resource "google_project_iam_member" "ci-k8s" {
  role   = "roles/container.developer"
  member = "serviceAccount:${google_service_account.gitlab-ci.email}"
}

resource "google_project_iam_member" "ci-custom" {
  role   = "projects/${var.gcp_project}/roles/${google_project_iam_custom_role.ci.role_id}"
  member = "serviceAccount:${google_service_account.gitlab-ci.email}"

  depends_on = ["google_project_iam_custom_role.ci"]
}

resource "google_service_account_key" "gitlab-ci" {
  service_account_id = "${google_service_account.gitlab-ci.name}"
}

resource "gitlab_project_variable" "gcp-service-account" {
  project = "${ var.gitlab_project }"
  key     = "GCP_SERVICE_ACCOUNT"
  value   = "${base64decode(google_service_account_key.gitlab-ci.private_key)}"
}

# GKE_CLUSTER_NAME
resource "gitlab_project_variable" "cluster-name" {
  project = "${ var.gitlab_project }"
  key     = "GKE_CLUSTER_NAME"
  value   = "${google_container_cluster.primary-cluster.name}"
}

resource "gitlab_project_variable" "cluster-zone" {
  project = "${ var.gitlab_project }"
  key     = "GKE_ZONE"
  value   = "${google_container_cluster.primary-cluster.location}"
}

resource "gitlab_project_variable" "gke-project" {
  project = "${ var.gitlab_project }"
  key     = "GKE_PROJECT"
  value   = "${var.gcp_project}"
}

resource "kubernetes_service_account" "gitlab-admin" {
  metadata {
    name      = "gitlab-admin"
    namespace = "kube-system"
  }
}

resource "kubernetes_role_binding" "gitlab-admin" {
  metadata {
    name      = "gitlab-admin"
    namespace = "default"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-admin"
    namespace = "kube-system"
  }
}

data "kubernetes_secret" "gitlab-admin" {
  metadata {
    name      = "${kubernetes_service_account.gitlab-admin.default_secret_name}"
    namespace = "kube-system"
  }

  depends_on = ["kubernetes_role_binding.gitlab-admin", "kubernetes_service_account.gitlab-admin"]
}

resource gitlab_project_cluster "primary-gke" {
  project                       = "${var.gitlab_project}"
  name                          = "${google_container_cluster.primary-cluster.name}"
  enabled                       = true
  kubernetes_api_url            = "https://${google_container_cluster.primary-cluster.endpoint}"
  kubernetes_token              = "${lookup(data.kubernetes_secret.gitlab-admin.data,"token")}"
  kubernetes_ca_cert            = "${base64decode(google_container_cluster.primary-cluster.master_auth.0.cluster_ca_certificate)}"
  kubernetes_namespace          = "default"
  kubernetes_authorization_type = "rbac"
  environment_scope             = "*"
}
