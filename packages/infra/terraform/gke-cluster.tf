resource "google_container_cluster" "primary-cluster" {
  name     = "primary-app-cluster"
  location = "australia-southeast1"

  min_master_version = "1.13.6-gke.13"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true

  initial_node_count = 1

  # Setting an empty username and password explicitly disables basic auth
  master_auth {
    username = ""
    password = ""
  }

  addons_config {
    kubernetes_dashboard {
      disabled = true # deprecated?
    }
  }

  lifecycle {
    ignore_changes = [
      "master_auth.0.client_key",
      "master_auth.0.client_certificate_config",
      "master_auth.0.cluster_ca_certificate",
      "master_auth.0.client_certificate",
      "network",
    ]
  }
}

resource "google_container_node_pool" "primary-cluster_preemptible_nodes" {
  name     = "project-k8s-node-pool"
  location = "australia-southeast1"
  cluster  = "${google_container_cluster.primary-cluster.name}"

  node_count = 1

  autoscaling {
    max_node_count = 3
    min_node_count = 0
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    preemptible  = true
    machine_type = "g1-small"

    metadata {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",

      # Allows Traefik to respond to DNS challenge
      "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    ]
  }

  lifecycle {
    ignore_changes = [
      "node_count",
    ]
  }
}

data "google_dns_managed_zone" "project-cloud-dns-zone" {
  name = "${ var.managed_zone_name }"
}

resource "google_compute_address" "wildcard-k8s-address" {
  name = "wildcard-k8s-ip"
}

resource "google_dns_record_set" "base-record" {
  name = "${data.google_dns_managed_zone.project-cloud-dns-zone.dns_name}"

  type = "A"
  ttl  = 300

  managed_zone = "${data.google_dns_managed_zone.project-cloud-dns-zone.name}"

  rrdatas = ["${google_compute_address.wildcard-k8s-address.address}"]
}

resource "google_dns_record_set" "wildcard-record" {
  name = "*.${data.google_dns_managed_zone.project-cloud-dns-zone.dns_name}"

  type = "A"
  ttl  = 300

  managed_zone = "${data.google_dns_managed_zone.project-cloud-dns-zone.name}"

  rrdatas = ["${google_compute_address.wildcard-k8s-address.address}"]
}

output "wildcard_ip" {
  value = "${google_compute_address.wildcard-k8s-address.address}"
}

output "k8s_endpoint" {
  value = "${google_container_cluster.primary-cluster.endpoint}"
}
