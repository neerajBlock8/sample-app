variable "managed_zone_name" {
  type = "string"
}

variable "gcp_project" {
  type = "string"
}

variable "gitlab_token" {
  type = "string"
}

variable "gitlab_project" {
  type = "string"
}
