terraform {
  backend "gcs" {
    bucket = "block8-sample-app-1-tf-state"
    prefix = "terraform/state"
  }
}

provider "gitlab" {
  token = "${var.gitlab_token}"
}

provider "google" {
  project = "${var.gcp_project}"
  region  = "australia-southeast1"
}

provider "kubernetes" {
  host = "${google_container_cluster.primary-cluster.endpoint}"

  client_certificate     = "${base64decode(google_container_cluster.primary-cluster.master_auth.0.client_certificate)}"
  client_key             = "${base64decode(google_container_cluster.primary-cluster.master_auth.0.client_key)}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.primary-cluster.master_auth.0.cluster_ca_certificate)}"
}

provider "helm" {
  kubernetes {
    host = "${google_container_cluster.primary-cluster.endpoint}"

    client_certificate     = "${base64decode(google_container_cluster.primary-cluster.master_auth.0.client_certificate)}"
    client_key             = "${base64decode(google_container_cluster.primary-cluster.master_auth.0.client_key)}"
    cluster_ca_certificate = "${base64decode(google_container_cluster.primary-cluster.master_auth.0.cluster_ca_certificate)}"
  }

  max_history     = 200
  install_tiller  = true
  service_account = "${kubernetes_service_account.tiller_service_account.metadata.0.name}"
}

resource "null_resource" "helm-upgrade" {
  provisioner "local-exec" {
    command = "gcloud beta container clusters get-credentials primary-app-cluster --region australia-southeast1 --project ${var.gcp_project} && helm init --upgrade"
  }
}
