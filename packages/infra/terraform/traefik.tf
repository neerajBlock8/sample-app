resource "helm_release" "traefik" {
  name  = "traefik"
  chart = "${path.module}/../traefik-chart"
  wait  = false

  set {
    name  = "gcpProject"
    value = "${var.gcp_project}"
  }

  set {
    name  = "letsEncryptEmailAddress"
    value = "neeraj.gupta@block8.io"
  }

  set {
    name  = "baseDomain"
    value = "${google_dns_record_set.base-record.name}"
  }

  set {
    name  = "loadBalancerIP"
    value = "${google_compute_address.wildcard-k8s-address.address}"
  }

  depends_on = ["null_resource.helm-upgrade", "gitlab_project_cluster.primary-gke"]
}
