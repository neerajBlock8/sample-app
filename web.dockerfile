# docker build -t gcr.io/block8-cwaaaa/web -f web.dockerfile .
# docker push gcr.io/block8-cwaaaa/web

# Stage 1 - the build process

# build phase

FROM node:alpine as build-deps

WORKDIR  '/app'

COPY ./packages/web/package.json ./

RUN npm install

COPY packages/web .

RUN npm run build

# Stage 2 - the production environment
FROM nginx:1.12-alpine

COPY --from=build-deps /app/build /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY packages/web/nginx.conf /etc/nginx/conf.d

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]



